<?php

Route::group(['middleware' => 'web', 'prefix' => 'core', 'namespace' => 'HMS\Core\Http\Controllers'], function()
{
    Route::get('/', 'CoreController@index');
});
