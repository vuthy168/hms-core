<?php

Route::group(['middleware' => 'web', 'prefix' => 'user', 'namespace' => 'HMS\User\Http\Controllers'], function()
{
    Route::get('/', 'UserController@index');
});
