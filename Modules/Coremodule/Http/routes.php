<?php

Route::group(['middleware' => 'web', 'prefix' => 'coremodule', 'namespace' => 'HMS\Coremodule\Http\Controllers'], function()
{
    Route::get('/', 'CoremoduleController@index');
});
