<?php

Route::group(['middleware' => 'web', 'prefix' => 'hmscore', 'namespace' => 'HMS\Hmscore\Http\Controllers'], function()
{
    Route::get('/', 'HmscoreController@index');
});
