<?php

Route::group(['middleware' => 'web', 'prefix' => 'accountant', 'namespace' => 'HMS\Accountant\Http\Controllers'], function()
{
    Route::get('/', 'AccountantController@index');
});
